﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joro.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default/Index?1=value&b=value
        public ActionResult Index(int a = 0, int b= 0, string operation = "add")
        {
            ViewBag.Title = "Примерно съдържание";

            ViewBag.p = Request.Params;

            ViewBag.a =a; //a 
            ViewBag.b =b; //b
            ViewBag.Operation = operation;
            ViewBag.result = ViewBag.a + ViewBag.b;

            Calculate(a, b, operation);

            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form) 
        {
            ViewBag.p = Request.Params;
            ViewBag.a = int.Parse(Request.Params["a"]);
            ViewBag.b = int.Parse(Request.Params["b"]);
            ViewBag.Operation = Request.Params["operation"];

            Calculate(ViewBag.a, ViewBag.b, ViewBag.Operation);


            return View();
        }

        private void Calculate(int a, int b, string operation)
        {
            switch (operation)
            {
                case "add":
                    ViewBag.result = Calculator.Add(a, b);
                    break;
                case "sub":
                    ViewBag.result = Calculator.Subtract(a, b);
                    break;
                case "mul":
                    ViewBag.result = Calculator.Multiply(a, b);
                    break;
                case "div":
                    ViewBag.result = Calculator.Divide(a, b);
                    break;
                default:
                    ViewBag.result = "wat u doing man";
                    break;
            }
        }
    }
    
}